﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IronPython.Hosting;
using FraudDetection.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace FraudDetection.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        public async Task<ActionResult> Index( int train , int test)
        {
    
            var model = await new TransactionData().GetInitialTransactions(train , test);
            return View(model);
        }


        [HttpPost]
        public async Task<ActionResult> GetResult()
        {

            var model = await new TransactionData().GetFinalTransactions();
            
            return View("Index", model);
        }

        public ActionResult Transaction()
        {
            var transactions = new List<SelectListItem>
            {
                new SelectListItem {Text = "5000" , Value ="5000" },
                new SelectListItem {Text = "10000" , Value ="10000" },
                new SelectListItem {Text = "100000" , Value = "100000" },
            };
            var testTransactions = new List<SelectListItem>
            {
                new SelectListItem {Text = "100" , Value ="100" },
                new SelectListItem {Text = "500" , Value ="500" },
                new SelectListItem {Text = "1000" , Value ="1000" },
            };

            ViewBag.TransactionsNumbers = new SelectList(transactions , "Text", "Value");
            ViewBag.TestTransactionsNumbers = new SelectList(testTransactions, "Text", "Value");

            return View();
        }

        [HttpPost]
        public ActionResult Transaction(GeneratorModel model)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index", "Home", new
                {
                    train = model.TransactionsNumber,
                    test = model.TestTransactionsNumber
                });
            }
            return View();
        }


    }
    public class TransactionData
    {

        public async Task<IEnumerable<TransactionModel>> GetInitialTransactions( int train, int test)
        {
            string apiUrl = String.Format("http://localhost:5000/initial/{0}/{1}", train, test);
            var result = new List<TransactionModel>();
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TransactionModel>>(data);
              
                }

                
            }
            return result;
        }
        public async Task<IEnumerable<TransactionModel>> GetFinalTransactions()
        {
            string apiUrl = "http://localhost:5000/result";
            var result = new List<TransactionModel>();
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TransactionModel>>(data);

                }


            }
            return result;
        }
    }
}
