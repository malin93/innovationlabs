﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FraudDetection.Models
{
    // Models returned by MeController actions.

    public class TransactionModel
    {
        public int index { get; set; }
        public double OutletCode { get; set; }
        public double CompanyCode { get; set; }
        public double TransactionType { get; set; }
        public double TransactionSource { get; set; }
        public double DescriptionChannel { get; set; }
        public double CardType { get; set; }
        public string FormatedCED { get; set; }
        public string FormatedDT { get; set; }
        public double FormatedAmount { get; set; }
        public double Probability { get; set; }

        public string Color { get; set; }

    }

}