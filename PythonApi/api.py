from flask import Flask , jsonify , request
import fraud_detection
import pdb

app = Flask(__name__)

@app.route('/initial/<train>/<test>')
def index(train , test):
    train , test = int(train) , int(test)
    df = fraud_detection.get_apidata(train , test)


    return df.reset_index().to_json(double_precision =2 , orient='records')

@app.route('/result')
def result():
    df = fraud_detection.get_result()
    return df.reset_index().to_json(double_precision=2, orient='records')


if __name__ == '__main__':
    app.run(debug=True)