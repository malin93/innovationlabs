import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier,GradientBoostingClassifier,RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn import linear_model, datasets
from sklearn.linear_model import RandomizedLogisticRegression
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn import tree
import random_data
import random_date_generator

reg_model = RandomForestClassifier(criterion='gini',n_estimators=200)
train_data, test_data  = pd.DataFrame , pd.DataFrame
formated_testData = pd.DataFrame


def get_data(train , test):
    global train_data, test_data , formated_testData
    x , y  = random_data.load_testing(size=9 , length=train+test)
    columns = ['Amount' , 'OutletCode' , 'CompanyCode' , 'TransactionType', 'CardExpiryDate',
               'TransactionSource' , 'DescriptionChannel' , 'CardType' , 'DateTime']
    df = pd.DataFrame(data=x , columns= columns)
    df ['Class'] = pd.Series(data=y)
    train_data, test_data = split_data(df, test)
    formated_testData = test_data.copy()
    formated_testData = format_test_data(formated_testData)

def format_data(df):
    transform_date = lambda x: random_date_generator.randomDate("01/01/2017 1:30:00", "15/02/2017 4:50:23", x)
    df['FormatedCED'] = df['CardExpiryDate'].map(transform_date)
    df['FormatedDT'] = df['DateTime'].map(transform_date)
    df['FormatedAmount'] = df['Amount'] * 400
    return df

def split_data(df , n):
    procent = n/len(df)
    train_data, test_data = train_test_split(df, test_size=procent)

    return train_data, test_data

def format_test_data(df):
    df =  format_data(df)
    df = df.drop(['DateTime' , 'Amount','CardExpiryDate', 'Class'],axis =1)
    return df


def train_model():
    global reg_model ,train_data

    y = train_data['Class']
    x = train_data.drop(['Class'], axis =1)

    reg_model.fit(x,y)

def predict():
    global reg_model , test_data
    test_x = test_data.drop(['Class'], axis =1)
    predicted = reg_model.predict(test_x)
    predicted_proba = reg_model.predict_proba(test_x)

    return predicted, predicted_proba


def get_apidata(train , test):
    global formated_testData
    get_data(train, test)
    print(train , test)
    return formated_testData



def get_result():

    global formated_testData , train_data
    if  train_data.empty:
        get_apidata()
    train_model()
    predicted , proba = predict()
    color = lambda x :  "90EE90" if x <0.5 else "F08080"
    formated_testData['Probability'] = proba.T[1]
    formated_testData['Color'] = formated_testData['Probability'].map(color)

    return formated_testData


