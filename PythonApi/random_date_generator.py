import random
import time

def strTimeProp(start, end, format, prop):


    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format, time.localtime(ptime))


def randomDate(start, end, prop):
    return strTimeProp(start, end, '%d/%m/%Y %H:%M:%S', prop)

#print ()randomDate("1/1/2008 1:30 PM", "1/1/2009 4:50 AM", random.random())

